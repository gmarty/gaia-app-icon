'use strict';

window.GaiaAppIcon = (function(exports) {
  const LAUNCH_TIMEOUT = 500;

  const PREDEFINED_ICONS = {
    default: 'default_icon.png',
    unrecoverable: 'app_install_unrecoverable.svg',
    installing: 'app_installing.svg',
    paused: 'app_install_canceled.svg',
    failed: 'app_install_failed.svg'
  };

  const SHADOW_BLUR = 1;
  const SHADOW_OFFSET = { x: 1, y: 1 };
  const SHADOW_COLOR = 'rgba(0, 0, 0, 0.2)';
  const DEFAULT_BACKGROUND_COLOR = 'rgb(228, 234, 238)';
  const CANVAS_PADDING = 2 * window.devicePixelRatio;
  const FAVICON_SCALE = 0.55;
  const ICON_FETCH_TIMEOUT = 10000;

  var proto = Object.create(HTMLElement.prototype);

  // Allow the base URL to be overridden
  var baseurl = window.GaiaAppIconBaseurl || 'gaia-app-icon/';

  proto.createdCallback = function() {
    this._template = template.content.cloneNode(true);

    var shadow = this.createShadowRoot();
    shadow.appendChild(this._template);

    this._container = shadow.getElementById('image-container');
    this._subtitle = shadow.getElementById('subtitle');
    this._image = null;
    this._app = null;
    this._bookmark = null;
    this._entryPoint = '';
    this._size = 0;
    this._hasIcon = false;
    this._hasUserSetIcon = false;
    this._hasPredefinedIcon = true;

    this._predefinedIcons = {};
    for (var key in PREDEFINED_ICONS) {
      this._predefinedIcons[key] = baseurl + 'images/' + PREDEFINED_ICONS[key];
    }

    this.refresh();
  };

  Object.defineProperty(proto, 'app', {
    get: function() {
      return this._app;
    },

    set: function(app) {
      if (this._app) {
        this._app.removeEventListener('progress', this);
        this._app.removeEventListener('downloaderror', this);
        this._app.removeEventListener('downloadsuccess', this);
        this._app.removeEventListener('downloadapplied', this);
      }

      this._cancelIconLoad();
      this._removeOldIcon();
      this._hasIcon = false;

      this._app = app;
      if (!app) {
        return;
      }

      this._bookmark = null;
      this._app.addEventListener('progress', this);
      this._app.addEventListener('downloaderror', this);
      this._app.addEventListener('downloadsuccess', this);
      this._app.addEventListener('downloadapplied', this);

      this._container.classList.add('initial-load');
    },

    enumerable: true
  });

  Object.defineProperty(proto, 'entryPoint', {
    get: function() {
      return this._entryPoint;
    },

    set: function(entryPoint) {
      this._entryPoint = entryPoint ? entryPoint : '';
      this.refresh();
    },

    enumerable: true
  });

  Object.defineProperty(proto, 'bookmark', {
    get: function() {
      return this._bookmark;
    },

    set: function(bookmark) {
      this.app = null;
      this._bookmark = bookmark;
    },

    enumerable: true
  });

  Object.defineProperty(proto, 'icon', {
    get: function() {
      return new Promise((resolve, reject) => {
        if (!this._hasIcon || this._hasPredefinedIcon) {
          reject();
        }

        var image = this._container.querySelector('img');
        if (!image) {
          reject();
        }

        var canvas = document.createElement('canvas');
        canvas.width = canvas.height = this._size;
        var ctx = canvas.getContext('2d', { willReadFrequently: true });
        ctx.drawImage(image, 0, 0, this._size, this._size);
        try {
          canvas.toBlob((blob) => {
            resolve(blob);
          });
        } catch(e) {
          console.error('Failed to get canvas data for icon');
          reject(e);
        }
      });
    },

    set: function(blob) {
      if (blob) {
        this._relayout();
        this._prepareIconLoader();
        this._hasUserSetIcon = true;
        this._hasPredefinedIcon = false;
        this._image.src = URL.createObjectURL(blob);
      } else {
        this._cancelIconLoad();
        this._removeOldIcon();
      }
    },

    enumerable: true
  });

  proto._cancelIconLoad = function() {
    if (!this._image) {
      return;
    }

    this._image.onload = null;
    this._image.src = '';
    this._image = null;
  };

  proto._removeOldIcon = function() {
    while (this._container.children.length > 1) {
      this._container.removeChild(this._container.lastChild);
    }
  };

  proto.launch = function() {
    if (!this.app && !this.bookmark) {
      return;
    }

    this.classList.add('launching');
    setTimeout(() => { this.classList.remove('launching'); }, LAUNCH_TIMEOUT);

    if (this.app) {
      this.app.launch(this.entryPoint);
    } else {
      window.open(this.bookmark.url, '_blank');
    }
  };

  proto._prepareIconLoader = function() {
    // Create new image element to load icon
    this._cancelIconLoad();
    this._hasPredefinedIcon = true;
    this._hasUserSetIcon = false;
    this._image = document.createElement('img');

    this._image.onload = () => {
      if (!this._hasUserSetIcon) {
        // Process the image
        var canvas = document.createElement('canvas');
        canvas.width = canvas.height = this._size;

        var ctx = canvas.getContext('2d', { willReadFrequently: true });
        ctx.shadowColor = SHADOW_COLOR;
        ctx.shadowBlur = SHADOW_BLUR;
        ctx.shadowOffsetX = SHADOW_OFFSET.x;
        ctx.shadowOffsetY = SHADOW_OFFSET.y;

        if (!this._hasPredefinedIcon &&
            this.bookmark && this._image.width <= this._size / 2) {
          // Draw filled background circle
          ctx.beginPath();
          ctx.arc(this._size / 2, this._size / 2,
                  this._size / 2 - CANVAS_PADDING, 0, 2 * Math.PI);
          ctx.fillStyle = DEFAULT_BACKGROUND_COLOR;
          ctx.fill();

          // Disable shadow drawing and image smoothing
          ctx.shadowBlur = 0;
          ctx.shadowOffsetY = 0;
          ctx.mozImageSmoothingEnabled = false;

          var iconSize = this._size * FAVICON_SCALE;
          ctx.drawImage(this._image,
                        (this._size - iconSize) / 2,
                        (this._size - iconSize) / 2,
                        iconSize, iconSize);
        } else {
          if (!this._hasPredefinedIcon && this.bookmark) {
            // Clip the favicon in a circle
            ctx.beginPath();
            ctx.arc(this._size / 2, this._size / 2,
                    this._size / 2 - CANVAS_PADDING, 0, 2 * Math.PI);
            ctx.clip();
          }
          var padding = this._hasUserSetIcon ? 0 : CANVAS_PADDING;
          ctx.drawImage(this._image,
                        padding, padding,
                        this._size - padding * 2,
                        this._size - padding * 2);
        }

        canvas.toBlob(function(image, blob) {
          if (image.onload) {
            image.src = URL.createObjectURL(blob);
          }
        }.bind(this, this._image));
      }

      this._image.onload = () => {
        // Add new icon
        this._removeOldIcon();
        this._container.style.height = '';
        this._container.appendChild(this._image);
        this._container.classList.remove('initial-load');
        this._image = null;
        this._hasIcon = true;

        if (this._pendingIconRefresh) {
          this._pendingIconRefresh = false;
          this.refresh();
        } else if (!this._hasPredefinedIcon) {
          this.dispatchEvent(new CustomEvent('icon-loaded'));
        }
      }

      if (this._hasUserSetIcon) {
        this._image.onload();
      }
    };

    this._image.onerror = (e) => {
      console.error('Failed to load icon', e);
      if (this._pendingIconRefresh) {
        this._pendingIconRefresh = false;
        this._image = null;
        this.refresh();
      } else if (!this._hasIcon) {
        this._hasPredefinedIcon = true;
        this._image.src = this._predefinedIcons.default;
      } else {
        this._image = null;
      }
    };
  },

  proto._relayout = function() {
    this._size = this.clientWidth;
    this._container.style.height = this._size + 'px';
    this._size *= window.devicePixelRatio;
  },

  proto.refresh = function() {
    if (!this._template) {
      return;
    }

    this._relayout();
    this._container.classList.remove('downloading');

    var manifest =
      this.app ? (this.app.manifest || this.app.updateManifest) : null;
    var name = '';
    if (manifest) {
      var manifestName = (manifest.entry_points && this.entryPoint) ?
        manifest.entry_points[this.entryPoint] : manifest;
      name = manifestName.short_name || manifestName.name;
    } else if (this.bookmark) {
      name = this.bookmark.name;
    }
    this._subtitle.textContent = name;

    if (this._size < 1 || (!this.app && !this.bookmark)) {
      return;
    }

    // If we're already loading an icon, let that finish first.
    if (this._image) {
      this._pendingIconRefresh = true;
      return;
    }
    this._prepareIconLoader();

    // Handle icon loading for bookmarks, app icon loading is more involved
    // and handled below this block.
    if (this.bookmark) {
      if (this.bookmark.icon) {
        this._hasPredefinedIcon = false;

        // It'd be great to just set the src here, but we need to be able
        // to read-back the image, so use system XHR.
        var xhr = new XMLHttpRequest({ mozAnon: true, mozSystem: true });

        xhr.open('GET', this.bookmark.icon, true);
        xhr.responseType = 'blob';
        xhr.timeout = ICON_FETCH_TIMEOUT;

        try {
          xhr.send();

          xhr.onload = function load(image) {
            if (!image.onload) {
              return;
            }

            if (xhr.status !== 0 && xhr.status !== 200) {
              image.onerror(xhr.status);
            } else {
              image.src = URL.createObjectURL(xhr.response);
            }
          }.bind(this, this._image);
          return;
        } catch(e) {
          console.error('Error loading bookmark icon', e);
        }
      }

      // Fallback to the default icon
      if (!this._hasIcon) {
        this._image.src = this._predefinedIcons.default;
      }
      return;
    }

    // If the app is downloading/paused/failed to download, use a predefined
    // icon.
    if (this.app.installState === 'pending' &&
        !this.app.downloadAvailable &&
        !this.app.readyToApplyDownload) {
      // App is in an unrecoverable state
      this._image.src = this._predefinedIcons.unrecoverable;
    } else if (this.app.downloading) {
      // App is downloading
      this._image.src = this._predefinedIcons.installing;
      this._container.classList.add('downloading');
    } else if (this.app.downloadError) {
      if (this.app.downloadError.name === 'DOWNLOAD_CANCELED') {
        // Download was cancelled by the user
        this._image.src = this._predefinedIcons.paused;
      } else {
        // Download was cancelled by an error
        this._image.src = this._predefinedIcons.failed;
      }
    } else if (this.app.installState === 'pending') {
      // Download was paused
      this._image.src = this._predefinedIcons.paused;
    } else {
      this._hasPredefinedIcon = false;
      navigator.mozApps.mgmt.getIcon(this.app, this._size, this.entryPoint).
        then(function(image, blob) {
          if (image.onload) {
            image.src = URL.createObjectURL(blob);
          }
        }.bind(this, this._image),
        function(image, e) {
          console.error('Failed to retrieve icon', e);
          if (image.onload && !this._hasIcon) {
            image.src = this._predefinedIcons.default;
          }
        }.bind(this, this._image));
    }
  };

  proto.handleEvent = function(e) {
    switch(e.type) {
    case 'progress':
    case 'downloadsuccess':
      break;

    case 'downloaderror':
    case 'downloadapplied':
      this.refresh();
      break;
    }
  };

  var template = document.createElement('template');
  var stylesheet = baseurl + 'style.css';
  template.innerHTML =
    `<style>@import url(${stylesheet});</style>
     <div id='image-container'><div id="spinner"></div></div>
     <div><div id='subtitle'></div></div>`;

  return document.registerElement('gaia-app-icon', { prototype: proto });
})(window);
